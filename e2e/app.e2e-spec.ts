import { TestNewAppPage } from './app.po';

describe('test-new-app App', () => {
  let page: TestNewAppPage;

  beforeEach(() => {
    page = new TestNewAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
